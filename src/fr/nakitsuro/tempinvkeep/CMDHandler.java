package fr.nakitsuro.tempinvkeep;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CMDHandler implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String arg2, String[] args) {
		if (command.getName().equalsIgnoreCase("keepinv")) {
			if (args.length > 0) {
				if (sender.hasPermission(Main.plugin.perm)) {
					@SuppressWarnings("deprecation")
					OfflinePlayer opl = Bukkit.getOfflinePlayer(args[0]);
					if (!opl.hasPlayedBefore() && !opl.isOnline()) {
						sender.sendMessage(Messages.getMessage("no-player"));
						return true;
					}
					PlayerData pd = PlayerData.getData(opl.getUniqueId());
					if (args.length > 1) {
						int i = 0;
						try {
							i = Integer.parseInt(args[1]);
						} catch (Exception ex) {
							sender.sendMessage(Messages.getMessage("int-error"));
							return true;
						}
						pd.add(i);
						sender.sendMessage(Messages.getMessage("credited"));
					} else {
						sender.sendMessage(Messages.getMessage("info").replace("{player}", opl.getName()).replace("{number}", pd.getKeepInvs() + ""));
					}
					return true;
				}
			}
			if (sender instanceof Player) {
				Player p = (Player) sender;
				PlayerData pd = PlayerData.getData(p.getUniqueId());
				if (pd.getKeepInvs() > 0) {
					sender.sendMessage(Messages.getMessage("self-info").replace("{number}", pd.getKeepInvs() + ""));
				} else {
					sender.sendMessage(Messages.getMessage("self-noremaining"));
				}
			} else {
				sender.sendMessage(Messages.getMessage("help"));
			}
		}
		return true;
	}

}
