package fr.nakitsuro.tempinvkeep;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

public class EventsHandler implements Listener {
	
	@EventHandler
	public void onDeath(PlayerDeathEvent e) {
		PlayerData pd = PlayerData.getData(e.getEntity().getUniqueId());
		if (pd.canKeepInventory()) {
			e.setKeepInventory(true);
			pd.consume();
			e.getEntity().sendMessage(Messages.getMessage("used"));
		}
	}

}
