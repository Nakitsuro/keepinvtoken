package fr.nakitsuro.tempinvkeep;

import java.io.IOException;
import java.util.ArrayList;

import org.bukkit.permissions.Permission;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import io.bluecube.thunderbolt.Thunderbolt;
import io.bluecube.thunderbolt.exceptions.FileLoadException;
import io.bluecube.thunderbolt.io.ThunderFile;

public class Main extends JavaPlugin {

	public static Main plugin;
	public ArrayList<PlayerData> data = new ArrayList<PlayerData>();
	public Permission perm = new Permission("tempinvkeep.give");
	public ThunderFile tf;
	
	@Override
	public void onEnable() {
		plugin = this;
		Messages.initMessages(this);
		this.getCommand("keepinv").setExecutor(new CMDHandler());
		try {
			tf = Thunderbolt.load("data", this.getDataFolder().getAbsolutePath());
		} catch (FileLoadException | IOException e) {
			e.printStackTrace();
		}
		PlayerData.load(tf);
		PluginManager pm = this.getServer().getPluginManager();
		if (!pm.getPermissions().contains(perm)) pm.addPermission(perm);
		pm.registerEvents(new EventsHandler(), this);
	}
	
	@Override
	public void onDisable() {
		PlayerData.save(tf);
		plugin = null;
	}

}
