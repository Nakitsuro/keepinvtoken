package fr.nakitsuro.tempinvkeep;

import java.io.File;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

public class Messages {
	
	static FileConfiguration language = null;
	static JavaPlugin plugin;
	
	public static void initMessages(JavaPlugin plugin) {
		Messages.plugin = plugin;
		Messages.getConfig();
	}
	
	public static String getMessage(String label) {
		String message = ChatColor.RED + "An error occured while trying to get a message. Please reffer to an Administator this error (Error with msg '" + label + "')";
		if (language == null)
			language = getConfig();
		String s = language.getString(label);
		if (s != null) {
			message = language.getString("prefix") + s;
		}
		message = ChatColor.translateAlternateColorCodes('&', message);
		return message;
	}

	private static FileConfiguration getConfig() {
		FileConfiguration config = null;
		File file = new File(Messages.plugin.getDataFolder(), "language.yml");
		if (!file.exists()) {
			file.getParentFile().mkdirs();
			Messages.plugin.saveResource("language.yml", false);
		}
		config = YamlConfiguration.loadConfiguration(file);
		return config;
	}
	

}
