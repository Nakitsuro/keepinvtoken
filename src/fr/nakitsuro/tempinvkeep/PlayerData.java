package fr.nakitsuro.tempinvkeep;

import java.io.IOException;
import java.util.UUID;

import io.bluecube.thunderbolt.io.ThunderFile;
import lombok.Data;

@Data
public class PlayerData {

	private UUID uuid;
	private int keepInvs;

	public PlayerData(UUID uuid) {
		this.uuid = uuid;
		this.keepInvs = 0;
	}

	public PlayerData(UUID uuid, int keepInvs) {
		this.uuid = uuid;
		this.keepInvs = keepInvs;
	}

	public PlayerData add(int i) {
		this.keepInvs = this.keepInvs + i;
		return this;
	}

	public boolean canKeepInventory() {
		if (this.keepInvs > 0)
			return true;
		return false;
	}

	public PlayerData consume() {
		this.keepInvs--;
		return this;
	}

	public static PlayerData getData(UUID uuid) {
		for (PlayerData pd : Main.plugin.data) {
			if (pd.getUuid().equals(uuid))
				return pd;
		}
		PlayerData pd = new PlayerData(uuid);
		Main.plugin.data.add(pd);
		return pd;
	}

	public static void load(ThunderFile tf) {
		for (String s : tf.keySet()) {
			UUID uuid = UUID.fromString(s);
			int i = tf.getInt(s);
			PlayerData pd = new PlayerData(uuid, i);
			Main.plugin.data.add(pd);
		}
	}

	public static void save(ThunderFile tf) {
		for (PlayerData pd : Main.plugin.data) {
			tf.set(pd.getUuid().toString(), pd.getKeepInvs());
		}
		try {
			tf.save();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
